﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule12
{
    public delegate void PropertyeventHandler(object sender, EvenArgsChanged e);
    interface IPropertychanged
    {
        event PropertyeventHandler Propertychanged;

    }
}
