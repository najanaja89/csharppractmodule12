﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule12
{
    class Program
    {
        static void Main(string[] args)
        {
            EvenArgsChanged e = new EvenArgsChanged();
            PropetyChanged propety = new PropetyChanged();
            propety.Propertychanged += propety.Changed;
            propety.Changed((PropetyChanged)propety, e);
            Console.ReadLine();
        }
    }
}
