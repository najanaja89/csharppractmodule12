﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule12
{
    class PropetyChanged : IPropertychanged
    {
        public event PropertyeventHandler Propertychanged;

        public void Changed(Object sender, EvenArgsChanged e)
        {
            Console.WriteLine($"Propety changed {e.TimeReached}");
        }
    }
}
