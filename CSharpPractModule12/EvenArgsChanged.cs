﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule12
{
    public class EvenArgsChanged : EventArgs
    {
        public DateTime TimeReached { get; set; }

        public EvenArgsChanged()
        {
            TimeReached = DateTime.Now;
        }
    }
}
